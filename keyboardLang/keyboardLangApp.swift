//
//  keyboardLangApp.swift
//  keyboardLang
//
//  Created by Gus on 2021/6/8.
//

import SwiftUI

@main
struct keyboardLangApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
