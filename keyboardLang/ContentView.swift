//
//  ContentView.swift
//  keyboardLang
//
//  Created by Gus on 2021/6/8.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, Try them!")
            .padding()
        LangTextFieldUI("zh-Hant")
            .frame(height: 30)
            .padding()
        LangTextFieldUI("en")
            .frame(height: 30)
            .padding()
        LangTextFieldUI("fr")
            .frame(height: 30)
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
