//
//  ZhuyinKeyboard.swift
//  keyboardLang
//
//  Created by Gus on 2021/6/8.
//

import Foundation
import SwiftUI

struct LangTextFieldUI: UIViewRepresentable {
    var languageCode: String
    init(_ languageCode: String) {
        self.languageCode = languageCode
    }
    func makeUIView(context: Context) -> UITextField {
        let textField = ZhHantTextField()
        textField.languageCode = self.languageCode
        textField.borderStyle = .roundedRect
        textField.placeholder = self.languageCode

        return textField
    }

    func updateUIView(_ view: UITextField, context: Context) {

    }
}

class ZhHantTextField: UITextField {
    func tryLoggingPrimaryLanguageInfoOnKeyboard() {
        print("Total number of keyboards. : \(UITextInputMode.activeInputModes.count)")
        for keyboardInputModes in UITextInputMode.activeInputModes{
            if let language = keyboardInputModes.primaryLanguage{
               dump(language)
            }
        }
    }
    
    var languageCode: String?{
        didSet {
            if self.isFirstResponder{
                self.resignFirstResponder();
                self.becomeFirstResponder();
            }
        }
    }
    
    override var textInputMode: UITextInputMode?{
        print("Total number of keyboards. : \(UITextInputMode.activeInputModes.count)")
        if let languageCode = self.languageCode {
            for keyboardInputModes in UITextInputMode.activeInputModes {
                if let language = keyboardInputModes.primaryLanguage {
                    
                    if language == languageCode {
                        print("success")
                        return keyboardInputModes;
                    }
                   
                }
            }
        }
        print("failed")
        return super.textInputMode;
    }
}
